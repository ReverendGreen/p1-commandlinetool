//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

// Function Declaration
int factorialRecurse(int inVal);

int main (int argc, const char* argv[])
{

    // insert code here...
    
    int numToFactorial;
    
    // Getting value from User with a prompt.
    std::cout << "Please enter a poitive number to find the factorial of that number! \n";
    // Obtaining value.
    std::cin >> numToFactorial;
    //Feeding value into function.
   
    std::cout << factorialRecurse(numToFactorial);
    // Obtaining value.

    
    
    return 0;
}



int factorialRecurse(int inVal)
{
    
    if (inVal > 0)
    {
        int outVal = inVal - 1;
        return inVal * factorialRecurse(outVal);
    }
    
     else
     {
         return 1;
     }
   
    
}
